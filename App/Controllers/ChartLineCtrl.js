angular.module('AgenceApp').controller('ChartLineCtrl', ['$scope', function($scope) {

	
	  $scope.labels = [];
	  $scope.data = [];
	  $scope.consultores = [];

	  $scope.$on('InvoiceChanges', function(events, args){
	    $scope.consultorInvoices = args;
		if($scope.consultorInvoices.length > 0){
			angular.forEach($scope.consultorInvoices,(consultor,consultorNumber) => {

				$scope.consultores[consultorNumber] = consultor.Consultor.Username;
				if(!$scope.data[consultorNumber])
					$scope.data[consultorNumber] = [];

				angular.forEach(consultor.Invoices,(invoice,key) => {

					let index = $scope.labels.indexOf(invoice.InvoiceDate);
					if(index === -1)
					  	$scope.labels[$scope.labels.length] = invoice.InvoiceDate;

                    $scope.data[consultorNumber][key] = invoice.Price;
				});
			});
		}

		console.log($scope.data,$scope.labels,$scope.consultores);
	  });

	  function onClick(points,evt){
	  	console.log(points, evt);
	  }

	  $scope.onClick = onClick;

}]);