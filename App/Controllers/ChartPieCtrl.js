angular.module('AgenceApp').controller('ChartPieCtrl', ['$scope','$filter', function($scope,$filter) {


	$scope.labels = [];
  	$scope.data = [];
  	$scope.consultores = [];

	$scope.consultorInvoices = [];

	$scope.$on('InvoiceChanges', function(events, args){

	    $scope.consultorInvoices = args;
	    
		if($scope.consultorInvoices.length > 0){

			let gananciaNetaTotal = 0;
			angular.forEach($scope.consultorInvoices,(consultor,consultorNumber) =>{
				gananciaNetaTotal+=consultor.TotalPrice;
			});

			angular.forEach($scope.consultorInvoices,(consultor,consultorNumber) =>{
				$scope.labels[consultorNumber] = consultor.Consultor.Username;
				$scope.data[consultorNumber] = parseInt((consultor.TotalPrice/gananciaNetaTotal)*100);
			});
		}
	});

}]);