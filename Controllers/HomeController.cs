﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agence.Data;
using MySql.Data.MySqlClient;
using Agence.Domain.Entities;
using Agence.Web.Mutators;
using System.Web.Http;
using Agence.Web.Models;

namespace Agence.Web.Controllers
{
    public class HomeController : Controller
    {


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Consultores()
        {
            var consultores = new List<User>();
            var usuariosTipo = new List<int>() { 0,1,2 };
            using (var context = new AgenceContext())
            {
                var usersId = context.Permissions.Where(x => x.Active == "S" && x.SystemId == 1 && usuariosTipo.Contains(x.UserTypeId)).Select(x => x.UserId).ToList();
                consultores = context.Users.Where(x => usersId.Contains(x.Id)).ToList();
            }
            return Json(consultores, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpGet]
        public ActionResult Invoices([FromUri] string[] users,DateTime? dateFrom,DateTime? dateTo)
        {
            var retVal = new List<ConsultorVM>();
            
            // TODO : filtrar por rango de fechas
            // TODO : traer solo los primeros 6 meses para cada consultor...
            // TODO : traer solo 4 consultores a la vez.

            using (var context = new AgenceContext())
            {
                if (users == null || users.Length == 0)
                    return Json(retVal, JsonRequestBehavior.AllowGet);

                
                foreach (var userId in users)
                {

                    // obtenemos el cliente
                    var user = context.Users.FirstOrDefault(x => x.Id == userId);
                    if (user == null)
                        continue;

                    // obtenemos el salario del cliente
                    var salario = context.Salaries.FirstOrDefault(x => x.UserId == userId);

                    if (salario == null)
                        continue;

                    // obtenemos todas las OS de un cliente.
                    var serviceOrders = context.ServiceOrders.Where(x => x.UserId == userId).Select(x => x.Id).ToList();
                    if (!serviceOrders.Any())
                        continue;

                    // obtenemos todas las facturas de una empresa segun las ordenes de servicio de un cliente
                    var invoicesQuery = context.Invoices.Where(x => serviceOrders.Contains(x.ServiceOrderId));
                    if (dateFrom.HasValue) {
                        dateFrom = dateFrom.Value.AddDays(-1); // quizas se podria considerar restar un mes, debido a que el periodo es por mes/año
                        invoicesQuery = invoicesQuery.Where(x => x.EmissionDate >= dateFrom);
                    }
                    if (dateTo.HasValue) invoicesQuery = invoicesQuery.Where(x => x.EmissionDate <= dateTo.Value);

                    if (!invoicesQuery.Any())
                        continue;

                    var consultor = new ConsultorVM()
                    {
                        Consultor = user
                    };

                    // agrupamos según fecha y año de emisión
                    var invoiceGroups = (from invoices in invoicesQuery
                                         group invoices by new { month = invoices.EmissionDate.Month, year = invoices.EmissionDate.Year } into d
                                         select new
                                         {
                                             d.Key.month,
                                             d.Key.year,
                                             invoices = d.Select(x => x)
                                         }
                        ).OrderBy(x => x.year).ThenBy(x => x.month);



                    foreach (var group in invoiceGroups)
                    {
                        var invoices = group.invoices;
                        var invoiceVM = new InvoiceViewModel()
                        {
                            NetConsultorPrice = salario.Bruto, // corresponde al costo fijo...
                        };

                        foreach (var invoice in invoices)
                        {

                            var totalTax = invoice.TotalTax;
                            var price = invoice.Price; // sumamos los valores netos de la facturas para conseguir la "receita liquida"
                            
                            var valor = invoice.Price;
                            var commision = (valor - (valor * totalTax)) * invoice.Commission;
                           
                            invoiceVM.Commission += commision;
                            if (totalTax > 0) // en caso de que el campo (TOTAL_IMP_INC) sea mayor a 0, calculamos el porcentaje y lo restamos al precio
                                price -= (price / totalTax) / 100;

                            invoiceVM.Price += price;
                            invoiceVM.Total += invoice.Total;
                        }

                        invoiceVM.InvoiceDate = string.Format("{0}/{1}", group.month, group.year);
                        invoiceVM.Gains = invoiceVM.Price - (invoiceVM.Commission + invoiceVM.NetConsultorPrice); // ganancias o lucro..


                        consultor.TotalPrice += invoiceVM.Price;
                        consultor.TotalConsultorPrice += invoiceVM.NetConsultorPrice;
                        consultor.TotalCommision += invoiceVM.Commission;
                        consultor.TotalGains += invoiceVM.Gains;

                        
                        consultor.Invoices.Add(invoiceVM);
                    }
                    
                    retVal.Add(consultor);
                }
            }
            return Json(retVal, JsonRequestBehavior.AllowGet);
        }
        

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}