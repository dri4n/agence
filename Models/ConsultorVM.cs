﻿using Agence.Domain.Entities;
using Agence.Web.Mutators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agence.Web.Models
{
    public class ConsultorVM
    {
        public ConsultorVM()
        {
            Invoices = new List<InvoiceViewModel>();
        }

        public User Consultor;
        public List<InvoiceViewModel> Invoices;

        public float TotalPrice { get; internal set; }
        public float TotalConsultorPrice { get; internal set; }
        public float TotalCommision { get; internal set; }
        public float TotalGains { get; internal set; }
    }
}