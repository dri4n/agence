﻿
window.$ = window.jQuery = require('jquery');
require('moment');
require('angular');
require('angular-chart.js');

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
};

const AgenceApp = angular.module('AgenceApp', ['chart.js']);
require('./Controllers/ConsultoresCtrl.js');
require('./Controllers/ChartLineCtrl.js');
require('./Controllers/ChartPieCtrl.js');