﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Agence.Domain.Entities;

namespace Agence.Web.Mutators
{
    public static class InvoiceMutator
    {
        public static InvoiceViewModel ToViewModel(this Invoice dbModel)
        {
            var retVal = new InvoiceViewModel();
            
            var totalTax = dbModel.TotalTax;
            //if(totalTax > 0)
            //{
            //    var percetajeValue = (dbModel.Total / totalTax) / 100;
            //    retVal.Total = dbModel.Total - percetajeValue;
            //}
            //else
            //{
            //    retVal.Total = dbModel.Total;
            //}

            var valor = dbModel.Price;
            var commision = (valor - (valor * totalTax)) * dbModel.Commission;

            retVal.Commission = commision;
            retVal.EmissionDate = dbModel.EmissionDate;
            retVal.Id = dbModel.Id;

            return retVal;
        }
    }

    public class InvoiceViewModel
    {
        public string[] users;

        public int Id;

        public float TotalTax;
        public float Price;
        public float Commission;
        public float NetConsultorPrice;
        public float Gains;

        public DateTime EmissionDate;
        public string InvoiceDate;

        public IEnumerable<Invoice> Invoices { get; internal set; }
        public float Total { get; internal set; }
    }
}